#
# Copyright (C) 2023 E FOUNDATION
#
# SPDX-License-Identifier: Apache-2.0
#

# DLKM modules
DLKM_MODULES_ORIG := $(LOCAL_PATH)/modules
DLKM_MODULES_DEST := $(TARGET_COPY_OUT_VENDOR)/lib/modules

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(DLKM_MODULES_ORIG)/,$(DLKM_MODULES_DEST))

# vendor_boot modules
VENDOR_RAMDISK_KERNEL_MODULES_ORIG := $(LOCAL_PATH)/vnd_ramdisk_modules
VENDOR_RAMDISK_KERNEL_MODULES_DEST := $(TARGET_COPY_OUT_VENDOR_RAMDISK)/lib/modules

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(VENDOR_RAMDISK_KERNEL_MODULES_ORIG)/,$(VENDOR_RAMDISK_KERNEL_MODULES_DEST))
